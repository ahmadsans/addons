from odoo import fields, models, api, _
from odoo.exceptions import Warning


class LoanOrder(models.Model):
    _name = 'loan.order'
    _description = 'loan'

    # Automated loan order number with LO prefix
    name = fields.Char('LO Number/Name',
                       required=True,
                       copy=False,
                       index=True,
                       readonly=True,
                       default=lambda self: _('New')
                       )
    partner_id = fields.Many2one('res.partner', 'Customer Name')
    street = fields.Char(' ', related='partner_id.street')
    city = fields.Char(' ', related='partner_id.city')
    state_id = fields.Many2one('res.country.state', ' ',
                               related='partner_id.state_id')
    zip = fields.Char(' ', related='partner_id.zip')
    country_id = fields.Many2one('res.country', ' ',
                                 related='partner_id.country_id')
    date_order = fields.Datetime('Loan Date')
    starting_date = fields.Date('Starting Date')
    end_date = fields.Date('Ending Date')
    status = fields.Selection(string="Status",
                              selection=[
                                  ('draft', 'Draft'),
                                  ('confirm', 'Confirmed'),
                                  ('sent', 'Sent'),
                                  ('part_delivered', 'Partially Delivered'),
                                  ('delivered', 'Delivered'),
                                  ('part_returned', 'Partially Returned'),
                                  ('returned', 'Returned'),
                              ], default='draft')
    loan_order_ids = fields.One2many(
                     'loan.order.line',
                     'loan_order_id',
                     'Products',
                     )

    # Automating number sequence with ir.sequence
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code(
            'loan_order_squence') or _(New)
        res = super(LoanOrder, self).create(vals)
        return res

    # Functions for button and navbar status

    @api.one
    def button_draft(self):
        self.write({
            'status': 'draft'
        })

    @api.one
    def button_confirm(self):
        self.write({
            'status': 'confirm'
        })

    @api.one
    def button_sent(self):
        self.write({
            'status': 'sent'
        })

    @api.one
    def button_delivered(self):
        self.write({
            'status': 'delivered'
        })

    @api.one
    def button_returned(self):
        self.write({
            'status': 'returned'
        })


class LoanOrderLine(models.Model):
    _name = 'loan.order.line'
    _description = "loan_order_line"

    product_id = fields.Many2one('product.product', 'Product')
    name = fields.Char('Description')
    qty = fields.Float('Quantity')
    available = fields.Float('Free Stock')
    loan_order_id = fields.Many2one('loan.order', 'Loan')

    # Funtion to prevent loan of out of stock product
    @api.multi
    @api.onchange('product_id')
    def _onchange_field(self):
        if self.product_id:
            self.name = self.product_id.display_name
            self.available = self.product_id.qty_available
            if not self.available:
                raise Warning(
                    'Product {0} is out of stock'.format(self.name)
                )

    # Funtion to prevent loan more than available product
    @api.multi
    @api.onchange('qty')
    def _onchange_qty(self):
        if self.qty > self.available:
            raise Warning(
                'You can not loan more than available stock'
            )
