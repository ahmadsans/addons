{
    'name': 'Loan',
    'description': 'Loan Order',
    'author': 'Ahmad Sanusi',
    'depends': ['base'],
    'data': [
        'security/loan_security.xml',
        'security/ir.model.access.csv',
        'views/loan_menu.xml',
        'views/loan_view.xml',
        ],
    'application': True,
}
