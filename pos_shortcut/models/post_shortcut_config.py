from odoo import fields, models, api


class PosShortcutConfig(models.Model):
    _name = 'pos.shortcut.config'

    name = fields.Char('Name')
    code = fields.Char('Code')
    action = fields.Many2one('account.journal',
                             string="Action",
                             readonly=True)
