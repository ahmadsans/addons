from odoo import fields, models, api


class InheritedAccountJournal(models.Model):
    _inherit = 'account.journal'

    shortcut_payment = fields.Many2one('pos.shortcut.config',
                                       string='Shortcut')

    @api.multi
    def write(self, vals):
        res = super(InheritedAccountJournal, self).write(vals)
        if self.shortcut_payment:
            sc_config = self.env[
                'pos.shortcut.config'
                ].search([('name', '=', self.shortcut_payment.name)])
            sc_config.write({
                'action': self.id
            })
        return res
