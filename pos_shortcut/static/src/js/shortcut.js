odoo.define('pos_shortcut.pos_shortcut', function(require){
    "use strict";

    var new_screens = require('point_of_sale.screens');
    var pos_model = require('point_of_sale.models');
    var core = require('web.core');
    var QWeb = core.qweb;

    pos_model.load_fields('account.journal','shortcut_payment');
    pos_model.load_models([{
        model: 'pos.shortcut.config',
        fields: ['name','action', 'code'],
        loaded: function(self,result){
            $.each(result, function(key, value){
                self.model_result = result;
            });
        }
    }]);

    function search(key, array) {
        for (var i=0; i < array.length; i++){
            if (array[i].code === key){
                return array[i]
            }
        }
    }

    var ActionpadWidget = new_screens.ActionpadWidget.include({
        renderElement: function() {
            var self = this;
            this._super();
            $(document).keypress(function(event) {
                var pressed_key = String.fromCharCode(event.which);
                switch (pressed_key) {
                    case 'q':
                        self.gui.show_screen('clientlist');
                        break;
                    case 'p':
                        self.gui.show_screen('payment');
                }
            });
        } 
    });
    
    var PaymentScreenWidget = new_screens.PaymentScreenWidget.include({
        render_paymentmethods: function() {
            var self = this;
            var methods = $(QWeb.render('new-PaymentScreen-Paymentmethods', { widget:this }));
                methods.on('click','.paymentmethod',function(){
                    self.click_paymentmethods($(this).data('id'));
                });
            return methods;
        },

        renderElement: function(){
            var self = this;
            this._super();
            var shortcut_result = this.pos.model_result;
            if (shortcut_result){
                $(document).keyup(function(event) {
                    var key_up = event.which;
                    var key_string = key_up.toString();
                    var action_ids = search(key_string, shortcut_result);
                    if (action_ids) {
                        var action_id = action_ids.action[0];
                        var payment_button = $('div[data-id="' + action_id + '"]');
                        console.log(payment_button);
                        if (payment_button.length == 0) {
                            alert('That payment method currently unavailable.')
                        } else {
                            self.click_paymentmethods(action_id);
                        }
                    }
                });
            }
        }
    })
});