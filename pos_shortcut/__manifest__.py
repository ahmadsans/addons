{
    'name': 'POS Shortcut New',
    'version': '1.0',
    'depends': [
        'point_of_sale',
        'account',
        ],
    'author': 'Port Cities',
    'description': """
        Training Odoo Functional and Development.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': [
        'security/pos_shortcut_security.xml',
        'security/ir.model.access.csv',
        'views/jsdata.xml',
        'views/pos_shortcut_view.xml',
        'views/pos_menu.xml',
        'views/account_journal_view.xml'
        ],
    'qweb': [
        'static/src/xml/template.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': False,
}
