odoo.define('pos_custom.pos_custom', function(require){
    "use strict";

    var new_screens = require('point_of_sale.screens');
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var core = require('web.core');
    var QWeb = core.qweb;

    var ActionButtonWidget = new_screens.ActionButtonWidget.include({    
        renderElement: function(){
            var self = this;
            this._super();
            var control_button = document.getElementsByClassName('control-buttons')[0];
            var pos_session_id = '<div class="session-id">Session: ' + this.gui.pos.pos_session.name + '</div>';
            $('.control-buttons').after(pos_session_id);
        }
    });

    var OrderWidget = new_screens.OrderWidget.include({
        get_product_image_url: function(product) {
            return window.location.origin + '/web/image?model=product.product&field=image_small&id='+product.id
        },

        render_orderline: function(orderline) {
            var el_str  = QWeb.render('Orderline',{widget:this, line:orderline}); 
            var el_node = document.createElement('div');
            var img = this.get_product_image_url(orderline.product);
            var html_image = '<img src="'+img+'">';
                el_node.innerHTML = _.str.trim(el_str);
                el_node = el_node.childNodes[0];
            var new_html =  '<div class="container"><div class="rows"><div class="col-1">' + html_image + '</div><div class="col-2">' + el_node.innerHTML + '</div></div></div';
                el_node.innerHTML = new_html;
                el_node.orderline = orderline;
                el_node.addEventListener('click',this.line_click_handler);
            var el_lot_icon = el_node.querySelector('.line-lot-icon');
            if(el_lot_icon){
                el_lot_icon.addEventListener('click', (function() {
                    this.show_product_lot(orderline);
                }.bind(this)));
            }
            orderline.node = el_node;
            return el_node;                    
        }
    });

    var ClearOrderLine = new_screens.ActionButtonWidget.extend({
        template: 'ClearOrderLine',
    
        button_click: function(){
        var self = this;
        this.clear_button_fun();
        },
    
        clear_button_fun(){
        this.pos.delete_current_order();
        },
    });
    new_screens.define_action_button({'name': 'clear_button_fun','widget': ClearOrderLine,});
    
});