{
    'name': 'Custom Point of Sale',
    'description': 'Custom Point of Sale',
    'author': 'Ahmad Sanusi',
    'depends': ['base', 'point_of_sale'],
    'application': True,
    'data': [
        'views/jsdata.xml',
        ],
    'qweb': [
        'static/src/xml/button.xml',
    ]
}
