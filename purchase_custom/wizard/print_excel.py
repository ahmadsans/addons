from odoo import api, fields, models
import xlwt
from xlsxwriter.workbook import Workbook
from io import StringIO
import base64


class PrintExcel(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def generate_excel_report(self):
        filename = 'filename.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")
        worksheet = workbook.add_sheet('Sheet 1')
        style = xlwt.easyxf('font: bold True, name Arial;')
        worksheet.write_merge(0, 1, 0, 3, 'The Data', style)
        fp = StringIO()
        workbook.save(fp)
        record_id = self.env['wizard.excel.report'].create({
            'excel_file': base64.encodestring(
                fp.getvalue()),
            'file_name': filename,
        })
        fp.close()
        return {
            'view_mode': 'form',
            'res_id': record_id,
            'res_model': 'wizard.excel.report',
            'view_type': 'form',
            'type': 'ir.action.act_window',
            'context': context,
            'target': 'new',
        }


class WizardExcelReport(models.Model):
    _name = "wizard.excel.report"
    excel_file = fields.Binary('Excel file')
    file_name = fields.Char('Excel file', size=64)
