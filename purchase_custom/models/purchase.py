from odoo import api, models, fields, _
from odoo.exceptions import Warning
import datetime


class custom_po(models.Model):
    _inherit = 'purchase.order'

    source_doc = fields.Many2one('sale.order', 'Document Source')

    # Task 1 create supplier email and phone field use related
    supplier_phone = fields.Char('Phone', related='partner_id.phone')
    supplier_email = fields.Char('Email', related='partner_id.email')

    # Task 2 Create contact person
    # Use active id as default
    contact_person = fields.Many2one('res.users', string='Contact Person',
                                     default=lambda self: self.env.uid,
                                     required=True)

    # Task 3 calculate order date duration
    time_of_delivery = fields.Float('Duration (days)',
                                    compute='get_time_of_delivery')

    # Task 6 add vendor reference
    partner_ref = fields.Char('Vendor reference')

    # Task 9 add DP field and replace total
    down_payment = fields.Float('Down Payment (DP)')
    amount_total_dp = fields.Float('Total', compute='_get_amount_total')

    # Function fot task 3,
    # calculate duration using date_planed and date_order
    @api.one
    @api.depends('date_planned', 'date_order')
    def get_time_of_delivery(self):
        if not self.date_planned:
            self.date_planned = fields.Date.today()
        mpd = fields.Date.from_string(self.date_planned)
        do = fields.Date.from_string(self.date_order)
        tod = mpd - do
        self.time_of_delivery = tod.days

    # Function for task 4
    # button funtion to display related product
    @api.multi
    def action_view_related_products(self):
        return {
            'name': 'related_products',
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'list',
            'res_model': 'purchase.order.line',
        }

    # Task 6 extend create function
    # to automatic fill empty partner_ref with "No Supplier Reference"
    @api.model
    def create(self, vals):
        if not vals.get('partner_ref'):
            vals['partner_ref'] = "No supplier reference"
        return super(custom_po, self).create(vals)

    # Task 6 extend write function
    # to automatic fill empty partner_ref with "No Supplier Reference"
    @api.multi
    def write(self, vals):
        res = super(custom_po, self).write(vals)
        if not self.partner_ref:
            self.partner_ref = "No supplier reference"
        return res

    # Function to calculate amount total for Task 9
    @api.depends('amount_untaxed', "amount_tax", "down_payment")
    def _get_amount_total(self):
        self.amount_total_dp = (self.amount_untaxed +
                                self.amount_tax - self.down_payment)


class modif_product(models.Model):
    _inherit = 'product.product'

    @api.multi
    def write(self, vals):
        res = super(modif_product, self).write(vals)
        if not self.seller_ids:

            # Task 7 autofill seller id if empty
            # Comment out task 10 since can't be use with task 7 simultaniously

            values = self.env['res.partner'].search([('name', '=',
                                                    'no vendor')])
            self.write({
                'seller_ids': [(0, 0, {'name': values.id})]
            })

            # raise Warning(
            #     'You have to fill vendor at least one'
            # )
        return res


class sale_custom(models.Model):
    _inherit = 'sale.order'

    # Task 11 auto connect beetwen so and PO
    def state_rewrite(self, state):
        # Get PO with docuemnt source with the name of current SO
        source_doc = self.env['purchase.order'].search([('source_doc', '=',
                                                        self.name)])

        # Change PO state based on clicked button
        source_doc.write({
            'state': state
        })

        # Change SO state based on clicked button
        self.write({
            'state': state
        })

    # Override button set to quotation on sales order
    @api.one
    def action_draft2(self):
        self.state_rewrite('draft')

    # Override button cancel on sales order
    @api.one
    def action_cancel2(self):
        self.state_rewrite('cancel')
