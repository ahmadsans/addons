{
    'name': 'Purchase Training',
    'version': '1.0',
    'depends': [
        'purchase',
        'sale',
        ],
    'author': 'Port Cities',
    'description': """
        Training Odoo Functional and Development.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': [
        'views/purchase_view.xml',
        'reports/product_tag_report.xml',
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
}
