from odoo import http
from odoo.http import request


class Main(http.Controller):

    @http.route('/checkout', auth="user", website=True)
    def checkouts(slef, **kwargs):
        Checkout = request.env['library.checkout']
        checkouts = Checkout.search([])
        return request.render(
            'library_website.index',
            {'docs': checkouts}
        )

    @http.route('/checkout/<model("library.checkout"):doc>',
                auth="user", website=True)
    def chekout(self, doc, **kwarg):
        return http.request.render(
            'library_website.checkout',
            {'doc': doc}
        )
