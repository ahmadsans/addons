{
    'name': 'Library Website',
    'description': "Create and check book chekout requests.",
    'author': 'Ahmad Sanusi',
    'depends': [
        'library_checkout',
        'website'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/library_security.xml',
        'views/website_assets.xml',
        'views/library_member.xml',
        'views/helloworld_template.xml',
        'views/checkout_template.xml',
        'views/website_menu.xml',
    ]
}
