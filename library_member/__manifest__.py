{
    'name': 'Library members',
    'Description': 'Manage People who will able to borrow books.',
    'author': 'Ahmad Sanusi',
    'depends': ['library_app', 'mail'],
    'application': False,
    'data': [
        'views/book_view.xml',
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/member_view.xml',
        'views/library_menu.xml',
        'views/book_list_template.xml'
    ]
}
