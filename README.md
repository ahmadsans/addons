# Engineer Learning Task

demo: https://odoodemo.sanusi.id/  
User:pass : email@domain:pci

## purchase_custom
### Task 1-4, 6, 7, 9, 10 dan 15

Module name: Purchase Training  
Technical Name: purchase_custom  
Required module(s): purchase
  
Modify purchase order view by inhereting purchase.order model
  
What this module do:  
- Add Supplier's email and phone to form view (task 1)
- Add info who made the purchase order (Contact person) in form and tree view (task 2)
- Add order duration in form view (task 3)
- Add Supplier Refence  and auto set it with No Supplier Reference if empty (task 6)
- add Down Payment (DP) field and re calculate total with DP (task 9)  

![Purchase Order Form View](img/ss1.png "Purchase Order Form View")

- Add related button to show related product of the order (task 4)

![Purchase Order Related Item](img/ss3.png "Purchase Order Related Item") 

- Add Product tag to Print selection to print product tag with barcode (task 15)  

![Print Product tag](img/ss2.png "Print Product tag")

![Product tag with barcode](img/ss4.png "Product tag with barcode")

- Engineer learning task 7, auto fill vendor with no vendor on sav if left empty
- Engineer learning task 10, give warning on save if vendors left empty.
- since it's contradictive to each other, only one is enabled.

![Engineer Learning Task 7 and 10](img/ss10.png "Engineer learning task 7 and 10")


## stock_custom
### task 5

Module name: Stock Custom  
Technical Name: stock_custom
Required module(s): stock

Modify inventory overview form view by inhereting stock.picking

What this module do:
- Add Mark as Todo button
- Set confirmation day with today and confirmed by with current active user  
when Mark as Todo button is clicked.

![Inventory Overview](img/ss5.png)

## Loan_order
### Task 16

Module name: Loan Order  
Technical name: loan_order

With this module we can easily know which items we loan to which customer.

![Loan Order Tree view](img/ss7.png)
![Loan Order Form View](img/ss8.png)

Summary:
- Loan Order number is automatic regenerated in sequence
- Customer name (partner_id): many2one field to res.partner 
- Address: automatic generated based on related field of customer name
- Loan date: datetime field when thr loan order is created
- Starting date: date field, date when loaned items are sent
- Ending date: date field, date when loaned itesm are returned
- Product: one2many field which contain items that loaned, quantity, and availability.
- Product and Description will always the same.
- Quantity is float and manually inputed.
- Free Stock is total available product and will automatically filled when selecting product.
- button visibility is depend on the order status. On draft, only confirm button is visible. Button draft and send will be shown on confirmed and returned order. Button delivered and returned shown on sent order. Button returned shown on delivered order.

Loan Order has its own menu  
![Loan Order Menu](img/ss6.png)

### Creating Loan Order

![Loan Order Create](img/ss9.png)

Selecting Customer Name will automaticaly fill the address.  
Loan date, starting date, and ending date have to be filled manually.

Selecting product will automatically fill the description and free stock.  
Warning will be shown if try to select product will free stock 0, and when the quantity is larger than available product.