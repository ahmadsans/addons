from odoo import fields, models


class Partner(models.Model):
    _inherit = 'res.partner'
    published_book_ids = fields.One2many(
        'library.book',  # Related model
        'publisher_id',  # Field for "this" on related model
        string='Published Books'
    )
    books_ids = fields.Many2many(
        'library.book', string='Authored Books'
    )
