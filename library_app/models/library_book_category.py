from odoo import api, fields, models


class BookCategory(models.Model):
    _name = 'library.book.category'
    _description = 'Book category'
    _parent_store = True

    name = fields.Char(translate=True, required=True)
    highlighted_id = fields.Reference(
        [('library.book', 'Book'), ('res.partner', 'Author')],
    )

    # Hierarcy fields
    parent_id = fields.Many2one(
        'library.book.category',
        'Parent category',
        ondelete='restrict'
    )
    parent_path = fields.Char(index=True)

    # Optional
    child_ids = fields.One2many(
        'library.book.category',
        'parent_id',
        'Subcategories'
    )
