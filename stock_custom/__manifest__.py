{
    'name': 'Stock Custom',
    'version': '1.0',
    'depends': [
        'stock'
        ],
    'author': 'Port Cities',
    'description': """
        Training Odoo Functional and Development.
    """,
    'website': 'http://www.portcities.net',
    'category': 'Purchase',
    'sequence': 1,
    'data': [
        'views/stock_view.xml',
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
}
