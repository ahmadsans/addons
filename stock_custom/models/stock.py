from odoo import api, fields, models, _
import datetime


class custom_stock(models.Model):
    _inherit = 'stock.picking'

    # Task 5, confirmation by and date
    confirm_date = fields.Datetime('Confirmed Date')
    confirm_user = fields.Many2one('res.users', 'Confirmed by')

    @api.multi
    def mark_as_todo(self):
        self.confirm_date = fields.Datetime.now('Confirmed date')
        self.confirm_user = self.env['res.users'].browse(self.env.uid)
